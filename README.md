# Uh Oh
`uhoh` --- Notes, docs, scripts for Comma AI Three devkit.


Comma Three AI is hardware to connect to a vehicle for use with
free software `openpilot` AI for "self-driving" cars.

* https://comma.ai/

* https://github.com/commaai/openpilot


# Docs
See `docs/` directory for more info.


# Setup
The default OpenPilot setup requires having accounts with proprietary
services. This can be worked around.

## Setup SSH Without Proprietary Service on Comma Three
Here is HOWTO connect to a Comma Three device without using proprietary
services.



The official CommaAI SSH wiki docs are here, but they note they are not for the
current version of OpenPilot:

* https://github.com/commaai/openpilot/wiki/SSH

The CommaAI proxy that OpenPilot uses is, sadly, github.
This is to be avoided.
Another set of docs here:

* https://ssh.comma.ai/

The above docs are reference, but not how it will be performed below.
The hostname used will be `tici` as that is what the device is named upstream.


This is most easily done with a laptop or similar on the same wifi network
as the Comma Three.


This needs to be done on a new device, or a device that has been reset
to factory shipping. When the device boots up, go through all the initial
steps to get it connected to the Internet via wifi. Stop at the step
that reads `Choose Software to Install`.


At this point, the Comma Three is on the wifi network, and SSH is running
with a SHARED ROOT SSH KEY by default. Get the device's IP address from
DHCP logs on the DHCP server (e.g. router/firewall).


On the laptop, set up `~/.ssh/config` thusly, using the device's IP from DHCP
in place of example `192.168.1.100` below. Either port `22` or port `8022`
can be used, the device's SSH config has it listening on both:

```
Host tici
	User comma
	Hostname 192.168.1.100
	Identityfile ~/.ssh/key.pem
	Port 22
```

You need to get that SHARED ROOT SSH KEY from github to log into the account:

```
wget -O ~/.ssh/key.pem https://raw.githubusercontent.com/commaai/openpilot/master/tools/ssh/id_rsa
```

Then make sure your SSH permissions are happy:

```
chmod 700 ~/.ssh
chmod 600 ~/.ssh/key.pem ~/.ssh/config
```

Once that is set up, you should be able to SSH into the device thusly:

```
ssh tici
```

On the device, add your ssh public keys, at the `comma@tici:~$` prompt:

```
mkdir ~/.ssh
chmod 700 ~/.ssh/
```

Copy over laptop keys a variety of ways, such as from the laptop:

```
scp -p ~/.ssh/id_ed25519.pub tici:.ssh/authorized_keys
```

Note: Doing the OpenPilot install removes these keys, apparently.


Notes on SSH keys before OpenPilot is installed:


```
root@tici:~# grep ^AuthorizedKeysFile /etc/ssh/sshd_config
AuthorizedKeysFile /data/params/d/GithubSshKeys

root@tici:/# ls -l /data/params/
total 4
lrwxrwxrwx 1 comma comma   18 Feb  4 23:52 d -> /data/params/d_tmp
drwxr-xr-x 2 comma comma 4096 Feb  4 23:52 d_tmp

root@tici:~# cat /data/params/d_tmp/GithubSshKeys
from="10.0.0.0/8,172.16.0.0/12,192.168.0.0/16" ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+iXXq30Tq+J5NKat3KWHCzcmwZ55nGh6WggAqECa5CasBlM9VeROpVu3beA+5h0MibRgbD4DMtVXBt6gEvZ8nd04E7eLA9LTZyFDZ7SkSOVj4oXOQsT0GnJmKrASW5KslTWqVzTfo2XCtZ+004ikLxmyFeBO8NOcErW1pa8gFdQDToH9FrA7kgysic/XVESTOoe7XlzRoe/eZacEQ+jtnmFd21A4aEADkk00Ahjr0uKaJiLUAPatxs2icIXWpgYtfqqtaKF23wSt61OTu6cAwXbOWr3m+IUSRUO0IRzEIQS3z1jfd1svgzSgSSwZ1Lhj4AoKxIEAIc8qJrO4uymCJ public
```

Other keys of note. The `/data` dir is read-write, but `/etc` is read-only.
So it looks like it is doing an overlay with `rw` data from here, for
example with the SSH keys:

```
root@tici:~# ls -l /data/etc/ssh/
total 32
-rw------- 1 root root 1385 Feb  4 23:52 ssh_host_dsa_key
-rw-r--r-- 1 root root  599 Feb  4 23:52 ssh_host_dsa_key.pub
-rw------- 1 root root  505 Feb  4 23:52 ssh_host_ecdsa_key
-rw-r--r-- 1 root root  171 Feb  4 23:52 ssh_host_ecdsa_key.pub
-rw------- 1 root root  399 Feb  4 23:52 ssh_host_ed25519_key
-rw-r--r-- 1 root root   91 Feb  4 23:52 ssh_host_ed25519_key.pub
-rw------- 1 root root 2590 Feb  4 23:52 ssh_host_rsa_key
-rw-r--r-- 1 root root  563 Feb  4 23:52 ssh_host_rsa_key.pub
```

Do the install with the `https://openpilot.comma.ai` URL. Make sure
you have an active SSH connection to the device before doing the install,
or you will lose SSH access. If you do an install and reboot, you lose
SSH access.


Note, after OpenPilot is installed, the `/data/params/d/GithubSshKeys`
file is gone. This file needs to be recreated before closing any SSH
sessions, or you will lose access to the device and have to start over.
Instead of using the SHARED ROOT SSH KEY used by the Comma Three, use
a unique SSH key. On the laptop:


```
user@laptop:~$ ssh-keygen -t ed25519
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/user/.ssh/id_ed25519): /home/user/.ssh/id_ed25519-comma
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/user/.ssh/id_ed25519-comma
Your public key has been saved in /home/user/.ssh/id_ed25519-comma.pub
The key fingerprint is:
SHA256:IGVxoSP4EGlmBK4gpCTn8oBlMkoVCN1ENWlfx+RK83c user@laptop
The key's randomart image is:
+--[ED25519 256]--+
|BBOB+.*oo. o.    |
|XO*o.oo+  ..o    |
|O=+ o.+. .o..    |
|++ o o o.. +     |
|. . .   S . . . E|
|             . . |
|                 |
|                 |
|                 |
+----[SHA256]-----+

user@laptop:~$ cat ~/.ssh/id_ed25519-comma.pub 
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOmI1V0P6dSatrpAgkS9rfmkM1Z1ncAVpHJlLlKrgnTw user@laptop
```

Then take that pubkey created above, and recreate the
`/data/params/d/GithubSshKeys` file on the device:

```
from="10.0.0.0/8,172.16.0.0/12,192.168.0.0/16" ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOmI1V0P6dSatrpAgkS9rfmkM1Z1ncAVpHJlLlKrgnTw user@laptop
```

On the screen of the Comma Three, go to:

`Setup Icon (top left) --> Network --> Advanced --> Enable SSH`


Then you should be able to ssh with the new key setup.
Either port `22` or port `8022` can be used.
So set up something like this on the laptop in `~/.ssh/config`.
Call the `Host` something different than `tici` so if/when you go back and
forth between resets and installs, the SSH is set up for each. golly.

```
Host openpilot
        User comma
        Hostname 192.168.1.100
        Port 8022
        Identityfile ~/.ssh/id_ed25519-comma
```

Then just SSH there from the laptop:

```
user@laptop:~$ ssh openpilot
                      .~ssos+.
                    +8888888888i,   
                   {888888888888o.  
                   h8888888888888k 
                   t888888888s888k 
                    `t88888d/ h88k 
                       ```    h88l 
                             ,88k` 
                            .d8h` 
                           +d8h   
                        _+d8h`     
                      ;y8h+`       
                      |-`


Welcome to AGNOS (GNU/Linux 4.9.103+ aarch64)

  System information as of Sat 05 Feb 2022 12:44:08 AM UTC

  System load:  0.23              Temperature:            75.0 C
  Usage of /:   30.5% of 9.78GB   Processes:              586
  Memory usage: 23%               Users logged in:        1
  Swap usage:   0%                IPv4 address for wlan0: 192.168.1.100

Last login: Sat Feb  5 00:41:30 2022 from 192.168.1.101
```

This will survive reboot.


An `sshfs` mount can also be setup, on laptop:

```
sudo apt update
sudo apt install sshfs

mkdir -p ~/mnt

sshfs openpilot:/ ~/mnt/

ls -lh ~/mnt/
```


# Unofficial
This repository, documentation, and code is
unofficial, unaffiliated with Comma AI.


# License
GPLv3+.


Copyright (C) 2022, Jeff Moe

