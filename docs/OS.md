# Operating System
This thing is running Ubuntu 20.04.3 fork, not Android (!).
Fork is named `AGNOS`.


## GNU Userspace
Built with GNU, like Ubuntu and Debian.


## Linux Kernel
```
root@tici:~# uname -a
Linux tici 4.9.103+ #26 SMP PREEMPT Wed Dec 8 15:30:31 PST 2021 aarch64 aarch64 aarch64 Gx

root@tici:~# dmesg -T # select lines from the dmesg:

Linux version 4.9.103+ (batman@workstation-adeeb) (gcc version 8.2.1 20180802 (GNU Toolchain for the A-profile Architecture 8.2-2018-08 (arm-rel-8.23)) ) 1

Machine: Qualcomm Technologies, Inc. sda845 v2.1 TurboX-SOM_V01

efi: UEFI not found.

# And what do they use for entropy? Old seeds? :)
random: fast init done

# Kernel command line:
rcupdate.rcu_expedited=1 console=ttyMSM0,115200n8 earlycon=msm_geni_serial,0xA84000 androidboot.hardware=qcom androidboot.console=ttyMSM0 video=DSI-1:1080x2160@60e mdss_mdp.panel=0:dsi:0:dsi_ss_ea8074_fhd_cmd_display ehci-hcd.park=3 lpm_levels.sleep_disabled=1 service_locator.enable=1 androidboot.selinux=permissive firmware_class.path=/lib/firmware/updates net.ifnames=0 dyndbg="" root=/dev/sda6 androidboot.bootdevice=1d84000.ufshc androidboot.serialno=92d29dd androidboot.baseband=sda msm_drm.dsi_display0=dsi_fhd_ea8074_1080_cmd_display: androidboot.slot_suffix=_a skip_initramfs rootwait ro init=/sbin/init

# Tainted kernel!
snd_soc_wcd9xxx: module verification failed: signature and/or required key missing - tainting kernel

# Kernel Debug is on:
**********************************************************
**   NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE   **
**                                                      **
** trace_printk() being used. Allocating extra memory.  **
**                                                      **
** This means that this is a DEBUG kernel and it is     **
** unsafe for production use.                           **
**                                                      **
** If you see this message and you are not debugging    **
** the kernel, report this immediately to your vendor!  **
**                                                      **
**   NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE   **
**********************************************************

# Surely nothing could go wrong here....
SELinux:  Starting in permissive mode

# nom
SMP: Total of 8 processors activated.

Linux video capture interface: v2.00
dmi: Firmware registration failed.
Advanced Linux Sound Architecture Driver Initialized.
Bluetooth: Core ver 2.22
cpufreq: driver osm-cpufreq up and running
clk: clk_cpu_osm_driver_probe: OSM CPUFreq driver inited
reg-fixed-voltage 1.gpio-regulator: could not find pctldev for node /soc/qcom,spmi@c440000/qcom,pm8998@0/pinctrl@c000/camera_rear_dvdd_en/camera_rear_dvdd_
en_default, deferring probe
reg-fixed-voltage 2.gpio-regulator: could not find pctldev for node /soc/qcom,spmi@c440000/qcom,pm8998@0/pinctrl@c000/camera_dvdd_en/camera_dvdd_en_default
, deferring probe

SELinux:  Registering netfilter hooks
...

usbserial: USB Serial support registered for FTDI USB Serial Device
usbserial: USB Serial support registered for GSM modem (1-port)

# Seems to have a lot of stuff in the kernel that isn't needed.

ALSA device list:
No soundcards found.

# But does it matter?
SELinux: unrecognized netlink message: protocol=0 nlmsg_type=106 sclass=netlink_route_socket pig=605 comm=systemd-network

wlan: module is from the staging directory, the quality is unknown, you have been warned.


usb 1-1.1: GSM modem (1-port) converter now attached to ttyUSB0
option 1-1.1:1.1: GSM modem (1-port) converter detected
usb 1-1.1: GSM modem (1-port) converter now attached to ttyUSB1
option 1-1.1:1.2: GSM modem (1-port) converter detected
usb 1-1.1: GSM modem (1-port) converter now attached to ttyUSB2
option 1-1.1:1.3: GSM modem (1-port) converter detected
usb 1-1.1: GSM modem (1-port) converter now attached to ttyUSB3
qmi_wwan 1-1.1:1.4: cdc-wdm0: USB WDM device
```

