# Bugs
`Bugs?` --- The following were encountered, may be bugs. Notable for now.


```
# User comma has files in it's own read/write home directory
# that are owned by user root.
comma@tici:~$ ls -la
total 44
drwxr-xr-x 1 comma comma  100 Feb  4 18:09 .
drwxr-xr-x 1 root  root    60 Sep  7 12:37 ..
-rw-rw-r-- 1 root  root   254 Sep 20 15:01 .bash_aliases
-rw------- 1 comma comma   12 Feb  4 18:09 .bash_history
-rw-r--r-- 1 comma comma  220 Feb 25  2020 .bash_logout
-rw-rw-r-- 1 root  root   230 Sep 20 15:01 .bash_profile
-rw-r--r-- 1 comma comma 3771 Feb 25  2020 .bashrc
drwxr-xr-x 4 comma comma  100 Feb  4 17:49 .cache
drwxrwxr-x 1 comma comma   60 Sep  7 12:37 .config
-rw-rw-r-- 1 root  root    30 Sep 20 15:01 .gdbinit
-rw-rw-r-- 1 root  root   204 Sep 20 15:01 .gitconfig
-rw-rw-r-- 1 root  root     5 Sep 20 15:01 .gitignore
-rw-r--r-- 1 comma comma  807 Feb 25  2020 .profile
-rw-rw-r-- 1 root  root     0 Dec  8 12:36 .sudo_as_admin_successful
-rw-rw-r-- 1 root  root   749 Sep 20 15:01 .tmux.conf
-rw-rw-r-- 1 root  root   339 Sep 20 15:01 .vimrc
```

# Proprietary Connections
The device when running makes connections to Github (owned by Microsoft)
and Microsoft IPs directly. Note, this is post-install, after one reboot:

```
root@tici:~# netstat -apnt
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      3821/systemd-resolv 
tcp        0      0 0.0.0.0:8022            0.0.0.0:*               LISTEN      7386/sshd: /usr/sbi 
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      7386/sshd: /usr/sbi 
tcp        0      0 192.168.1.100:57398     140.82.114.4:443        ESTABLISHED 48481/git-remote-ht 
tcp        0      0 192.168.1.100:36802     20.188.93.37:443        ESTABLISHED 46183/selfdrive.ath 
tcp6       0      0 :::8022                 :::*                    LISTEN      7386/sshd: /usr/sbi 
tcp6       0      0 :::22                   :::*                    LISTEN      7386/sshd: /usr/sbi 
```

The process `selfdrive.athena.athenad` is connecting to `athena.comma.ai`
which resolves to `20.188.93.37`.
That code is called here:

* https://github.com/commaai/openpilot/blob/master/selfdrive/athena/athenad.py#L37

To fix that the variable `ATHENA_HOST` will need to be set to a private
cloud, and a server will need to be available there.


The access to IP `140.82.114.4` is likely a git pull/update ?


# DNS
Notable, how DNS resolving is done. This binary answers on port `53`:

`/lib/systemd/systemd-resolved`

```
cat /etc/resolv.conf

nameserver 127.0.0.53
options edns0 trust-ad
search lan
```

# SELinux
Some SELinux from `dmesg`:

```
root@tici:~# dmesg -T|grep -i selinux
[Fri Feb  4 17:46:35 2022] Kernel command line: rcupdate.rcu_expedited=1 console=ttyMSM0,115200n8 earlycon=msm_geni_serial,0xA84000 androidboot.hardware=qcom androidboot.console=ttyMSM0 video=DSI-1:1080x2160@60e mdss_mdp.panel=0:dsi:0:dsi_ss_ea8074_fhd_cmd_display ehci-hcd.park=3 lpm_levels.sleep_disabled=1 service_locator.enable=1 androidboot.selinux=permissive firmware_class.path=/lib/firmware/updates net.ifnames=0 dyndbg="" root=/dev/sda6 androidboot.bootdevice=1d84000.ufshc androidboot.serialno=92d29dd androidboot.baseband=sda msm_drm.dsi_display0=dsi_fhd_ea8074_1080_cmd_display: androidboot.slot_suffix=_a skip_initramfs rootwait ro init=/sbin/init
[Fri Feb  4 17:46:35 2022] SELinux:  Initializing.
[Fri Feb  4 17:46:35 2022] SELinux:  Starting in permissive mode
[Fri Feb  4 17:46:39 2022] SELinux:  Registering netfilter hooks
[Fri Feb  4 17:46:45 2022] systemd[1]: systemd 245.4-4ubuntu3.13 running in system mode. (+PAM +AUDIT +SELINUX +IMA +APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 +SECCOMP +BLKID +ELFUTILS +KMOD +IDN2 -IDN +PCRE2 default-hierarchy=hybrid)
[Fri Feb  4 17:46:46 2022] SELinux: unrecognized netlink message: protocol=0 nlmsg_type=106 sclass=netlink_route_socket pig=595 comm=systemd-network
```

# Misc
Random buglets, maybe.

* When on the device screen, in setup somewhere, there is a
button to view regulatory compliance. When you view it, you
can see most of it, but the bottom is obscured by a button,
and it doesn't scroll.

