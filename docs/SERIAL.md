# Serial Access to Comma Three
Charge the device fully without booting it (e.g. as when ded).
I used Pine64 PinePower charger with USB-C.


Note, this device appears to be very sensitive to cable quality
and length. So far the only cable that has worked has been the
short thick USB-C cable that shipped with the unit.


```
# USB device connects with serial FTDI.
# 115200 8N1, no flow control

minicom -D /dev/ttyUSB0
```

Root user is `comma`. Not sure what password is, but I logged in so
it was something easy if it is set. The `comma` has `sudo` access.

