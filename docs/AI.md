# Comma Three AI
Poking at what makes up the AI.

```
omma@tici:~$ ls -hl /data/openpilot/models/
total 29M
-rw-r--r-- 1 comma comma 895K Feb  4 17:23 dmonitoring_model_q.dlc
-rw-r--r-- 1 comma comma  28M Feb  4 17:23 supercombo.thneed

omma@tici:~$ strings /data/openpilot/models/dmonitoring_model_q.dlc | head -6
dlc.metadata
converter-command=snpe-onnx-to-dlc input_dim=None enable_strict_validation=False input_encoding=[] copyright_file=None validation_target=[] model_version=4e19be90-bd5b-485d-b79a-2462f7f1b49e debug=-1 disable_batchnorm_folding=False udo_config_paths=None dry_run=None input_type=[]
converter-version=1.41.0.2173
model-copyright=N/A
model-version=4e19be90-bd5b-485d-b79a-2462f7f1b49e
quantizer-command=snpe-dlc-quantize help=false version=false verbose=false quiet=false silent=false debug=[] debug1=false debug2=false debug3=false log-mask=[] log-file=[] log-dir=[] log-file-include-hostname=false input_dlc=[/home/batman/tmp/dm/dmonitoring_model.dlc] input_list=[/home/batman/one/xx/tools/snpe/compile_test_data/qsample_input_list_torch_wide.txt] no_weight_quantization=false output_dlc=[/home/batman/tmp/dm/dmonitoring_model_q.dlc] enable_hta=false hta_partitions=[] use_enhanced_quantizer=false use_adjusted_weights_quantizer=false optimizations=[] override_params=false udo_package_path=[] use_symmetric_quantize_weights=false bitwidth=[] weights_bitwidth=[] act_bitwidth=[] bias_bitwidth=[]


comma@tici:~$ strings /data/openpilot/models/supercombo.thneed |head -1
{"binaries": [{"length": 4920, "name": "activate_image"}, {"length": 4112, "name": "concatenation"}, {"length": 8036, "name": "concatenation_to_buffer_half"}, {"length": 11232, "name": "convolution_horizontal_reduced_reads"}, {"length": 10652, "name": "convolution_horizontal_reduced_reads_1x1"}, {"length": 7512, "name": "convolution_horizontal_reduced_reads_depthwise"}, {"length": 7208, "name": "convolution_horizontal_reduced_reads_depthwise_stride_1"}, {"length": 3500, "name": "crop_nonmultiple_4_channels"}, {"length": 2244, "name": "elementwise_product"}, {"length": 2240, "name": "elementwise_sub"}, {"length": 2568, "name": "elementwise_sum"}, {"length": 6284, "name": "fc_Wtx"}, {"length": 2452, "name": "flatten"}, {"length": 9528, "name": "image1d_to_buffer_half"}, {"length": 9632, "name": "image2d_to_buffer_float"}, {"length": 9632, "name": "image2d_to_buffer_half"}, {"length": 3484, "name": "permute_with_height_lastdim_3d"}, {"length": 3504, "name": "permute_with_width_lastdim_3d"}, {"length": 12104, "name": "zero_pad_image_float"}, {"length": 12104, "name": "zero_pad_image_half"}], "kernels": [{"args": ["\u0000\u0000\u0000\u0000", "\u00005dl
```

So `dmonitoring_model_q.dlc` was run thru a conversion process, where it started as a `snpe-onnx`.


## ONNX
"ONNX is an open format built to represent machine learning models."
A Linux Foundation project.

* https://en.wikipedia.org/wiki/Open_Neural_Network_Exchange

* https://onnx.ai/

