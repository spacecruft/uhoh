# Proprietary Comma AI
The Comma AI appears at first to be quite "open", but it is built upon
a lot of proprietary infrastruction. For example, in the docs on using
`ssh` to access the device, step one:

```
1. You need a GitHub account. Make one if you don't already have one.
```

From: https://github.com/commaai/openpilot/wiki/SSH


So despite being "open", work will need to be done to use this device
without proprietary tools. I'm guessing this issue will be found throughout
the toolchain as I dig deeper. It already needs a libre fork.


Perhaps a list starting here of proprietary parts:

* OpenSSH access to the device "needs" a Github account.

* Development on Github.

* Discord for chat.

* SSH on the device uses Microsoft proxy.

* Also has GSM device, doing who-knows-what.


# Uploads
Track down where these are going, uploaded by `/data/openpilot/selfdrive/loggerd/uploader.py`:

```
{"msg": "upload ('2022-02-04--19-44-04--9/qlog.bz2', '/data/media/0/realdata/2022-02-04--19-44-04--9/qlog.bz2') over wifi", "ctx": {"dongle_id": "4bbff91fc2312f89", "version": "0.8.12-release", "dirty": false, "device": "tici"}, "level": "DEBUG", "levelnum": 10, "name": "swaglog", "filename": "uploader.py", "lineno": 264, "pathname": "/data/openpilot/selfdrive/loggerd/uploader.py", "module": "uploader", "funcName": "main", "host": "tici", "process": 46141, "thread": 547871622992, "threadName": "MainThread", "created": 1644029647.7889073}
```

