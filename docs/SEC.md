# Comma Three Security
Small notes on Comma Three security.

# SSH Remote Access
It appears from their docs that most users connect to their device
using ssh with the hostname `ssh.comma.ai` which resolves to
`104.214.96.241`, with the whois record showing the owner as
`Microsoft Corporation (MSFT)`.


So basically the device phones home to Microsoft.


# SSH Keys
On the device, the root filesystem is mounted read-only.
There is a `/persist` directory that contains some SSH RSA
keys:

```
root@tici:~# date ; ls -Rl /persist/
Sun 23 Jan 2022 02:30:39 AM UTC
/persist/:
total 4
drwxrwxr-x 2 comma comma 4096 Jan 14 23:41 comma

/persist/comma:
total 8
-rw------- 1 comma comma 1679 Jan 14 23:41 id_rsa
-rw-rw-r-- 1 comma comma  451 Jan 14 23:41 id_rsa.pub
```

The keys were created before the device shipped, not on first
boot, if the file timestamp is correct. So that ssh key
is in cleartext through the supply chain.

## Uh
Ok, this is perhaps even more absurd reading the docs:

```
1. Download the private key from the openpilot repo.. Save the key file as a text file and name it something like key.pem.
```

From: https://github.com/commaai/openpilot/wiki/SSH


That key:
https://raw.githubusercontent.com/commaai/openpilot/master/tools/ssh/id_rsa

```
-----BEGIN RSA PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC+iXXq30Tq+J5N
Kat3KWHCzcmwZ55nGh6WggAqECa5CasBlM9VeROpVu3beA+5h0MibRgbD4DMtVXB
t6gEvZ8nd04E7eLA9LTZyFDZ7SkSOVj4oXOQsT0GnJmKrASW5KslTWqVzTfo2XCt
Z+004ikLxmyFeBO8NOcErW1pa8gFdQDToH9FrA7kgysic/XVESTOoe7XlzRoe/eZ
acEQ+jtnmFd21A4aEADkk00Ahjr0uKaJiLUAPatxs2icIXWpgYtfqqtaKF23wSt6
1OTu6cAwXbOWr3m+IUSRUO0IRzEIQS3z1jfd1svgzSgSSwZ1Lhj4AoKxIEAIc8qJ
rO4uymCJAgMBAAECggEBAISFevxHGdoL3Z5xkw6oO5SQKO2GxEeVhRzNgmu/HA+q
x8OryqD6O1CWY4037kft6iWxlwiLOdwna2P25ueVM3LxqdQH2KS4DmlCx+kq6FwC
gv063fQPMhC9LpWimvaQSPEC7VUPjQlo4tPY6sTTYBUOh0A1ihRm/x7juKuQCWix
Cq8C/DVnB1X4mGj+W3nJc5TwVJtgJbbiBrq6PWrhvB/3qmkxHRL7dU2SBb2iNRF1
LLY30dJx/cD73UDKNHrlrsjk3UJc29Mp4/MladKvUkRqNwlYxSuAtJV0nZ3+iFkL
s3adSTHdJpClQer45R51rFDlVsDz2ZBpb/hRNRoGDuECgYEA6A1EixLq7QYOh3cb
Xhyh3W4kpVvA/FPfKH1OMy3ONOD/Y9Oa+M/wthW1wSoRL2n+uuIW5OAhTIvIEivj
6bAZsTT3twrvOrvYu9rx9aln4p8BhyvdjeW4kS7T8FP5ol6LoOt2sTP3T1LOuJPO
uQvOjlKPKIMh3c3RFNWTnGzMPa0CgYEA0jNiPLxP3A2nrX0keKDI+VHuvOY88gdh
0W5BuLMLovOIDk9aQFIbBbMuW1OTjHKv9NK+Lrw+YbCFqOGf1dU/UN5gSyE8lX/Q
FsUGUqUZx574nJZnOIcy3ONOnQLcvHAQToLFAGUd7PWgP3CtHkt9hEv2koUwL4vo
ikTP1u9Gkc0CgYEA2apoWxPZrY963XLKBxNQecYxNbLFaWq67t3rFnKm9E8BAICi
4zUaE5J1tMVi7Vi9iks9Ml9SnNyZRQJKfQ+kaebHXbkyAaPmfv+26rqHKboA0uxA
nDOZVwXX45zBkp6g1sdHxJx8JLoGEnkC9eyvSi0C//tRLx86OhLErXwYcNkCf1it
VMRKrWYoXJTUNo6tRhvodM88UnnIo3u3CALjhgU4uC1RTMHV4ZCGBwiAOb8GozSl
s5YD1E1iKwEULloHnK6BIh6P5v8q7J6uf/xdqoKMjlWBHgq6/roxKvkSPA1DOZ3l
jTadcgKFnRUmc+JT9p/ZbCxkA/ALFg8++G+0ghECgYA8vG3M/utweLvq4RI7l7U7
b+i2BajfK2OmzNi/xugfeLjY6k2tfQGRuv6ppTjehtji2uvgDWkgjJUgPfZpir3I
RsVMUiFgloWGHETOy0Qvc5AwtqTJFLTD1Wza2uBilSVIEsg6Y83Gickh+ejOmEsY
6co17RFaAZHwGfCFFjO76Q==
-----END RSA PRIVATE KEY-----
```

I don't get how this isn't a really bad idea.
A shared ssh private key to many systems,
shared publicly?


More ssh key fun:

```
root@tici:~# grep ssh_host_ /etc/ssh/sshd_config
HostKey /data/etc/ssh/ssh_host_rsa_key
HostKey /data/etc/ssh/ssh_host_dsa_key
HostKey /data/etc/ssh/ssh_host_ecdsa_key
HostKey /data/etc/ssh/ssh_host_ed25519_key
root@tici:~# ls -l /data/etc/ssh/ssh_host_*
-rw------- 1 root root 1373 Sep  7 18:37 /data/etc/ssh/ssh_host_dsa_key
-rw-r--r-- 1 root root  599 Sep  7 18:37 /data/etc/ssh/ssh_host_dsa_key.pub
-rw------- 1 root root  505 Sep  7 18:37 /data/etc/ssh/ssh_host_ecdsa_key
-rw-r--r-- 1 root root  171 Sep  7 18:37 /data/etc/ssh/ssh_host_ecdsa_key.pub
-rw------- 1 root root  399 Sep  7 18:37 /data/etc/ssh/ssh_host_ed25519_key
-rw-r--r-- 1 root root   91 Sep  7 18:37 /data/etc/ssh/ssh_host_ed25519_key.pub
-rw------- 1 root root 2590 Sep  7 18:37 /data/etc/ssh/ssh_host_rsa_key
-rw-r--r-- 1 root root  563 Sep  7 18:37 /data/etc/ssh/ssh_host_rsa_key.pub
```

Those keys were generated at factory. Shared?

```
root@tici:~# sha256sum /data/etc/ssh/ssh_host_*
27f51bad028a16a44570590feb04ef82f58d2be85fd617619f0586f2c73a62b6  /data/etc/ssh/ssh_host_dsa_key
66ed353232f9826b51a4c95590e1b9246b7dfd9ff642c6a5a54bfcc90fdd7519  /data/etc/ssh/ssh_host_dsa_key.pub
441b79400802b9a0947f93383cd83fb2a3ed67b0c12b12b9b98c5c3e444bdc7e  /data/etc/ssh/ssh_host_ecdsa_key
9dfcbed0ddb3dcbc151375c96b4077ac401a97ea86d82953d178cbf92fe6cadc  /data/etc/ssh/ssh_host_ecdsa_key.pub
d7e7b3256dcf3f8a334f6bd68c5bf37b29d102a2952deea9902f3ad9accce140  /data/etc/ssh/ssh_host_ed25519_key
6df70068291b69055c969cc721025cc78ec49b34f210b3496584a20a49df8100  /data/etc/ssh/ssh_host_ed25519_key.pub
ba8b445792f1216ab53cdf34bce275bce956387b0f9874f515500e30cfdfb361  /data/etc/ssh/ssh_host_rsa_key
750f22eb6d020497f5a1c846f48bde33c7efb55479217b95103a7ebdb1136414  /data/etc/ssh/ssh_host_rsa_key.pub
```


# Listening
Listening TCP ports.

```
root@tici:~# netstat -pant
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      3957/systemd-resolv 
tcp        0      0 0.0.0.0:8022            0.0.0.0:*               LISTEN      7655/sshd: /usr/sbi 
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      7655/sshd: /usr/sbi 
tcp6       0      0 :::8022                 :::*                    LISTEN      7655/sshd: /usr/sbi 
tcp6       0      0 :::22                   :::*                    LISTEN      7655/sshd: /usr/sbi 

# Looks like a local DNS resolver and ssh is listening on two ports:
root@tici:~# grep Port /etc/ssh/sshd_config
Port 8022
Port 22
```

Listening UDP.

```
root@tici:~# netstat -panu
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
udp        0      0 127.0.0.53:53           0.0.0.0:*                           3957/systemd-resolv 
udp        0      0 127.0.0.1:323           0.0.0.0:*                           5508/chronyd        
udp6       0      0 ::1:323                 :::*                                5508/chronyd        
```

So local DNS resolver is listening for both tcp/udp and time sync
with `chrony`.


`IPv6` is in use.


# Filesystem

```
# /var filesystem is at 100%, maybe logfiles gone wild.
root@tici:~# df -h /var/
Filesystem      Size  Used Avail Use% Mounted on
tmpfs           128M  128M     0 100% /var

```

The `/var` mount does not appear to be just a temp mount, there
are old files there, e.g.:

```
root@tici:~# head /var/log/syslog.1 
Sep  7 18:37:27 tici kernel: [    0.000000] Booting Linux on physical CPU 0x0
```

# Random, or not?
Surely deterministic random numbers are secure?

```
root@tici:~# ls -l /var/lib/systemd/random-seed
-rw------- 1 root root 512 Sep  7 18:37 /var/lib/systemd/random-seed
```

