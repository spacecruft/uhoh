# OpenPilot App
The OpenPilot application part of the whole setup....


The main processes appear to be these:

```
   5338 ?        Ss     0:10 /usr/bin/tmux new-session -s comma -d /usr/comma/comma.sh
   5356 pts/0    Ss+    0:00  \_ /usr/bin/bash /data/openpilot/launch_chffrplus.sh
  43168 pts/0    S+     0:02      \_ /usr/local/pyenv/versions/3.8.10/bin/python3 ./manager.py
  46090 pts/1    Ssl+   0:52          \_ /usr/local/pyenv/versions/3.8.10/bin/python3 ./manager.py
  46134 pts/1    Sl+    0:26              \_ ./_ui
  46140 pts/1    Sl     0:00              \_ python -m selfdrive.athena.manage_athenad
  46183 pts/1    Sl     0:10              |   \_ selfdrive.athena.athenad
  46141 pts/1    Sl+    0:02              \_ ./_navd
  46142 pts/1    Sl+    0:09              \_ ./_soundd
  46143 pts/1    S+     0:00              \_ selfdrive.loggerd.deleter
  46145 pts/1    Sl+    0:04              \_ selfdrive.logmessaged
  46152 pts/1    Sl+    0:00              \_ selfdrive.pandad
  46223 pts/1    Sl+    3:56              |   \_ ./boardd 33001c001951393330393936
  46153 pts/1    Sl+    2:40              \_ selfdrive.thermald.thermald
  46157 pts/1    Sl+    0:00              \_ selfdrive.timezoned
  46158 pts/1    S+     0:00              \_ selfdrive.tombstoned
  46159 pts/1    Sl+    0:08              \_ selfdrive.updated
  46160 pts/1    Sl+    0:01              \_ selfdrive.loggerd.uploader


comma@tici:~$ head -1 /usr/comma/comma.sh
#/usr/bin/env bash
```

