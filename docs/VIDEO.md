# Video
While in use, the Comma Three creates files in this directory:

`/data/media/0/realdata/2022-02-04--19-44-04--10`


```
$ ls -lh /data/media/0/realdata/2022-02-04--19-44-04--10
total 108M
-rw-r--r-- 1 jebba jebba  51M Feb  4 19:54 ecamera.hevc
-rw-r--r-- 1 jebba jebba  50M Feb  4 19:54 fcamera.hevc
-rw-r--r-- 1 jebba jebba 1.5M Feb  4 19:54 qcamera.ts
-rw-r--r-- 1 jebba jebba 631K Feb  4 19:54 qlog.bz2
-rw-r--r-- 1 jebba jebba 5.5M Feb  4 19:54 rlog.bz2
```

## HEVC

The `.hevc` are video files in `High Efficiency Video Coding` format.

* https://en.wikipedia.org/wiki/High_Efficiency_Video_Coding

The files can be played with `mplayer`.
HEVC is also known as `H.265` and `MPEG-H Part 2`.

## Playback

This forces the correct frames per second:

```
# VLC
vlc --hevc-fps 20 ecamera.hevc

# mplayer / mpv
mplayer --no-correct-pts --fps=20 ecamera.hevc
```

