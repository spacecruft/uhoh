# Comma AI Comma Three Hardware
Eight core ARM 64 bit.


See also `docs/OS.md`.

```
root@tici:~# arch
aarch64

# Eight pretty ARM cores.
root@tici:~# cat /proc/cpuinfo 
Processor       : AArch64 Processor rev 12 (aarch64)
processor       : 0
BogoMIPS        : 38.40
Features        : fp asimd evtstrm aes pmull sha1 sha2 crc32 atomics fphp asimdhp
CPU implementer : 0x51
CPU architecture: 8
CPU variant     : 0x7
CPU part        : 0x803
CPU revision    : 12

...

processor       : 7
BogoMIPS        : 38.40
Features        : fp asimd evtstrm aes pmull sha1 sha2 crc32 atomics fphp asimdhp
CPU implementer : 0x51
CPU architecture: 8
CPU variant     : 0x6
CPU part        : 0x802
CPU revision    : 13

Hardware        : Qualcomm Technologies, Inc SDA845

root@tici:~# free -h
              total        used        free      shared  buff/cache   available
Mem:          3.5Gi       460Mi       2.5Gi       141Mi       597Mi       2.9Gi
Swap:            0B          0B          0B


root@tici:~# df -h
Filesystem      Size  Used Avail Use% Mounted on
/dev/root       9.8G  3.0G  6.3G  33% /
devtmpfs        1.7G     0  1.7G   0% /dev
tmpfs           1.8G     0  1.8G   0% /dev/shm
tmpfs           358M   40M  318M  12% /run
tmpfs           5.0M  4.0K  5.0M   1% /run/lock
tmpfs           1.8G     0  1.8G   0% /sys/fs/cgroup
tmpfs           150M   88K  150M   1% /tmp
tmpfs           128M   84M   45M  66% /var
/dev/sda10       12M   24K   11M   1% /systemrw
/dev/sda2        27M   24K   25M   1% /persist
/dev/sda12       30G  505M   28G   2% /data
/dev/sda11      108M   36K   99M   1% /cache
/dev/sde4       120M   43M   77M  36% /firmware
/dev/sde9        28M   20M  7.3M  73% /dsp
/dev/nvme0n1    916G   77M  870G   1% /data/media
overlay         150M   88K  150M   1% /home
tmpfs           358M  4.0K  358M   1% /run/user/1000

root@tici:~# lsusb 
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 003: ID bbaa:ddcc  
Bus 001 Device 004: ID 2c7c:0125 Quectel Wireless Solutions Co., Ltd. EC25 LTE modem
Bus 001 Device 002: ID 0000:0000  
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

# Hard Drive
## NVMe
Has nice 1TB NVMe HD. That drive doesn't have a partition table.

```
root@tici:~# nvme list
Node             SN                   Model                                    Namespace Usage                      Format           FW Rev  
---------------- -------------------- ---------------------------------------- --------- -------------------------- ---------------- --------
/dev/nvme0n1     S64ANG0R513592B      Samsung SSD 980 1TB                      1          16.72  GB /   1.00  TB    512   B +  0 B   1B4QFXO7

root@tici:~# grep nvme /etc/fstab 
/dev/nvme0n1 /data/media auto discard,nosuid,nodev,nofail,x-systemd.device-timeout=5s 0 0

root@tici:~# fdisk -l /dev/nvme0n1 
Disk /dev/nvme0n1: 931.53 GiB, 1000204886016 bytes, 1953525168 sectors
Disk model: Samsung SSD 980 1TB                     
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


```

## `/dev/sda`
The main root hard drive is a `Skhynix` model `H28S7Q302BMR`.

* https://www.kynix.com/Detail/1335322/H28S7Q302BMR.html

```
$ sudo fdisk -l /dev/sda
Disk /dev/sda: 53.98 GiB, 57940115456 bytes, 14145536 sectors
Disk model: H28S7Q302BMR    
Units: sectors of 1 * 4096 = 4096 bytes
Sector size (logical/physical): 4096 bytes / 4096 bytes
I/O size (minimum/optimal): 786432 bytes / 786432 bytes
Disklabel type: gpt
Disk identifier: 2EA26BA4-A75E-67EF-2508-38105E3F23B1

Device       Start      End Sectors  Size Type
/dev/sda1        6        7       2    8K unknown
/dev/sda2        8     8199    8192   32M unknown
/dev/sda3     8200     8455     256    1M unknown
/dev/sda4     8456     8583     128  512K unknown
/dev/sda5     8584     8711     128  512K unknown
/dev/sda6     8712  3045143 3036432 11.6G unknown
/dev/sda7  3045144  6081575 3036432 11.6G unknown
/dev/sda8  6081576  6114343   32768  128M unknown
/dev/sda9  6114344  6122535    8192   32M unknown
/dev/sda10 6122536  6126631    4096   16M unknown
/dev/sda11 6126632  6159399   32768  128M unknown
/dev/sda12 6159400 14145502 7986103 30.5G unknown
```


# Network

```
root@tici:~# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: bond0: <BROADCAST,MULTICAST,MASTER> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 2a:2e:60:dd:35:dd brd ff:ff:ff:ff:ff:ff
3: dummy0: <BROADCAST,NOARP> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether fa:1f:5e:9b:5e:6b brd ff:ff:ff:ff:ff:ff
4: ip_vti0@NONE: <NOARP> mtu 1364 qdisc noop state DOWN group default qlen 1
    link/ipip 0.0.0.0 brd 0.0.0.0
5: ip6_vti0@NONE: <NOARP> mtu 1500 qdisc noop state DOWN group default qlen 1
    link/tunnel6 :: brd ::
6: sit0@NONE: <NOARP> mtu 1480 qdisc noop state DOWN group default qlen 1
    link/sit 0.0.0.0 brd 0.0.0.0
7: ip6tnl0@NONE: <NOARP> mtu 1452 qdisc noop state DOWN group default qlen 1
    link/tunnel6 :: brd ::
8: rmnet_ipa0: <> mtu 2000 qdisc noop state DOWN group default qlen 1000
    link/[530] 
9: wwan0: <POINTOPOINT,MULTICAST,NOARP> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/none 
10: wlan0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 3000
    link/ether 00:0a:f5:e7:d1:93 brd ff:ff:ff:ff:ff:ff
11: p2p0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 3000
    link/ether 02:0a:f5:6e:d1:93 brd ff:ff:ff:ff:ff:ff
```

