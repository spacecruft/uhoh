# Comma AI Self Driving Car on Dirt Roads

* Drives amazingly well on dirt road, not perfect for sure.

* Too close to edge.

* Take into conditions of road, e.g. when rain, road is muddy, edge sucks.

* Sees edge of road sometimes on other side of ditch.

* Drives amazingly well on snowy dirt road.

* Min speed is 17mph ? Would be great to go 1-16mph too.

* Would be great to slow down on curves, keep low G in general.
  Drive without spilling a glass on curves.

* Saw deer path as edge of road.

* Can drive on private road ok.

* Slow down for cattle guards! (and railroad tracks et)

